---
title: Frugal Robot Control
# author:
#   - name: Samantha Csik
#     url: https://samanthacsik.github.io/
#     orcid: 0000-0002-5300-3075
#     affiliation: Master of Environmental Data Science Program @ The Bren School (UCSB) & The National Center for Ecological Analysis & Synthesis
#     affiliation-url: https://ucsb-meds.github.io/ 
# date: 10-24-2022
# categories: [Quarto, R, MEDS] # self-defined categories
image: HDS.jpg
description: "My PhD project: exploring the use of inexact solutions in QP-based control to develop more frugal controllers"
draft: false
---

## Project Description

todo


## Related Publications

todo