---
title: "1st Year Programming course @Phelma"
categories: [Programming] # self-defined categories
# author: 
#   - Sélim Chefchaouni Moussaoui
image: c.jpg

draft: false
---

## Fall 2024

Co-supervision of practical sessions for the C programming course at [Phelma](https://phelma.grenoble-inp.fr/).

The course website is available [here](https://ens_phelma.pages.ensimag.fr/www_info_1a_s1/) (in French).
