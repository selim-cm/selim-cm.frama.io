#!/bin/bash

X=${1%.bib}
echo  "
\select@language{english}
\citation{*}
\bibstyle{dgsplain}
\bibdata{$X}" > ${1%.bib}.aux
bibtex ${1%.bib}
pandoc -f latex  ${1%.bib}.bbl -o ${1%.bib}.md
rm ${1%.bib}.aux
rm ${1%.bib}.blg